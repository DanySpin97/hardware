# Copyright 2019 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=KhronosGroup pn=SPIRV-LLVM-Translator tag=v${PV/_p/-} ] \
    cmake \
    alternatives

SUMMARY="LLVM/SPIR-V Bi-Directional Translator"
DESCRIPTION="
Library and tool for translation between LLVM IR and SPIR-V.
"

LICENCES="UoI-NCSA"
SLOT="9"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-lang/llvm:${SLOT}
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DCMAKE_BUILD_TYPE:STRING=Release
    -DCMAKE_POSITION_INDEPENDENT_CODE:BOOL=TRUE
    -DLLVM_DIR:PATH=/usr/$(exhost --target)/lib/llvm/${SLOT}/lib/cmake/llvm
)

src_install() {
    cmake_src_install

    local arch_dependent_alternatives=(
        /usr/$(exhost --target)/include/LLVMSPIRVLib{,-${SLOT}}
        /usr/$(exhost --target)/lib/libLLVMSPIRVLib.a{,-${SLOT}}
        /usr/$(exhost --target)/lib/pkgconfig/LLVMSPIRVLib.pc{,-${SLOT}}
    )

    alternatives_for _$(exhost --target)_${PN} ${SLOT} ${SLOT} "${arch_dependent_alternatives[@]}"
}

