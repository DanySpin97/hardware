# Copyright 2009 Jonathan Dahan <jedahan@gmail.com>
# Copyright 2012-2015 Wouter van Kesteren <woutershep@gmail.com>
# Distributed under the terms of the GNU General Public License v2
# Based on 'microcode-data-20090330.ebuild', which is:
#     Copyright 1999-2009 Gentoo Foundation

require github [ user=intel project=Intel-Linux-Processor-Microcode-Data-Files tag=${PN/intel-/}-${PV} ] \
    flag-o-matic

SUMMARY="Intel processor microcode update data"
DESCRIPTION="
CPU microcode is a mechanism to correct certain errata in existing systems. The normal preferred
method to apply microcode updates is using the system BIOS, but for a subset of Intel's processors
this can be done at runtime using the operating system. This package contains microcode updates for
processors that support OS loading of microcode updates.

Linux can update processor microcode very early in the kernel boot sequence. In situations when the
BIOS update isn't available, early loading is the next best alternative to updating processor
microcode. Microcode states are reset on a power reset, hence, it is required to be updated every
time during the boot process. Loading microcode using the initrd method is recommended so that the
microcode is loaded at the earliest time for best coverage. Systems that cannot tolerate downtime
may use the late reload method to update a running system without a reboot.
"

LICENCES="intel-ucode"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build:
        sys-apps/iucode-tool
"

DEFAULT_SRC_INSTALL_EXTRA_DOCS=( releasenote )

src_prepare() {
    edo iucode_tool --write-earlyfw=microcode.cpio intel-ucode
}

src_install() {
    insinto /usr/$(exhost --target)/lib/firmware

    # install a small initramfs for use with CONFIG_MICROCODE_EARLY
    doins microcode.cpio

    # install the split binary ucode files (used by the kernel directly)
    doins -r intel-ucode

    # and try to run the update on boot for systems not configured to use a initramfs
    insinto /usr/$(exhost --target)/lib/tmpfiles.d
    hereins ${PN}.conf <<EOF
w /sys/devices/system/cpu/microcode/reload - - - - 1
EOF

    # hook up the microcode.cpio initrd in kernel-install
    exeinto /usr/$(exhost --target)/lib/kernel/install.d
    doexe "${FILES}"/95-microcode.install

    emagicdocs
}

pkg_postinst() {
    elog "The recommended and safest way to update the microcode is on early boot before running"
    elog "userspace by enabling the following kernel options:"
    elog ""
    elog "CONFIG_BLK_DEV_INITRD"
    elog "CONFIG_MICROCODE"
    elog "CONFIG_MICROCODE_EARLY (only for kernels < 4.10)"
    elog "CONFIG_MICROCODE_INTEL"
    elog ""
    elog "You can use /usr/$(exhost --target)/lib/firmware/microcode.cpio directly via the initrd"
    elog "kernel command line, add it to your existing initramfs, or via CONFIG_INITRAMFS_SOURCE."
}

